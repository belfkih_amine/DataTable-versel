import React, { useState } from "react";

interface Column {
  key: string;
  label: string;
  sortable?: boolean; // Ajout de la propriété sortable par colonne
  filterable?: boolean;
}

interface DataTableProps {
  data: any[]; // Données à afficher dans le tableau
  columns: Column[]; // Informations sur les colonnes
  sortable?: boolean; // Ajout de la prop sortable globale
  filterable?: boolean; 
}

const DataTable: React.FC<DataTableProps> = ({ data, columns, sortable, filterable }) => {
  const [sortedColumn, setSortedColumn] = useState<string | null>(null);
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [filters, setFilters] = useState<Record<string, string>>({});

  const handleSort = (key: string, sortableColumn: boolean | undefined) => {
    if (sortableColumn || sortable) {
      // Vérifiez si le tri est autorisé pour cette colonne ou global
      if (sortedColumn === key) {
        setSortOrder(sortOrder === "asc" ? "desc" : "asc");
      } else {
        setSortedColumn(key);
        setSortOrder("asc");
      }
    }
  };

  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const handleFilterChange = (key: string, value: string) => {
    setFilters((prevFilters) => ({ ...prevFilters, [key]: value }));
  };

  const filteredData = data.filter((row) =>
    // Filtrage par recherche
    Object.values(row).some((value) => {
      return String(value).toLowerCase().includes(searchTerm.toLowerCase());
    })
  ).filter((row) =>
    // Filtrage par filtres
     Object.entries(filters).every(([key, value]) => {
      const rowValue = row[key];
      console.log(rowValue, value, String(rowValue).toLowerCase().includes(value.toLowerCase()))
      return String(rowValue).toLowerCase().includes(value.toLowerCase());
    })
  );

  const sortedData = sortedColumn
  ? filteredData.slice().sort((a, b) => {
      const aValue = a[sortedColumn];
      const bValue = b[sortedColumn];
      if (aValue === bValue) return 0;
      return sortOrder === "asc"
        ? aValue < bValue
          ? -1
          : 1
        : aValue > bValue
        ? -1
        : 1;
    })
  : filteredData;

  



  return (
    <div className="overflow-x-auto">
      {/* Champ de recherche */}
      <input
        type="text"
        placeholder="Rechercher..."
        value={searchTerm}
        onChange={handleSearch}
        className="border border-gray-300 rounded px-3 py-1 mb-4"
      />      
      {/* Contrôles de filtrage par colonne */}
      {columns.map((column) => (
        <div key={column.key} className="mb-4">
          <label className="mr-2 font-bold">{column.label}</label>
          <input
            type="text"
            value={filters[column.key] || ''}
            onChange={(e) => handleFilterChange(column.key, e.target.value)}
            className="border border-gray-300 rounded px-3 py-1"
            placeholder={`Filtrer par ${column.label.toLowerCase()}...`}
          />
        </div>
      ))}
      <table className="table-auto w-full">
        <thead>
          <tr>
            {columns.map((column) => (
              <th
                key={column.key}
                onClick={() => handleSort(column.key, column.sortable)} // Vérifiez si le tri est autorisé pour cette colonne
                className={column.sortable || sortable ? "cursor-pointer" : ""} // Changez le curseur si le tri est autorisé
              >
                {column.label}
                {(column.filterable || filterable) && (column.sortable || sortable) &&
                  sortedColumn === column.key && (
                    <span>{sortOrder === "asc" ? " ↑" : " ↓"}</span>
                  )}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {sortedData.map((row, rowIndex) => (
            <tr key={rowIndex}>
              {columns.map((column) => (
                <td key={column.key}>{row[column.key]}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default DataTable;
